#! /usr/bin/env bash


PRIVATE_REPO=http://10.202.129.124:8081
PROJECT_NAME=otr
SERVICE_NAME=cdm-integration
IMAGE_NAME=${PRIVATE_REPO}/${PROJECT_NAME}/${SERVICE_NAME}
ENV=$2


function build-services {
    docker run --rm -v $(pwd):/opt/app -w /opt/app ${PRIVATE_REPO}/otr/gradle:jdk8 gradle build -x test -x pmdMain -x pmdTest
    docker login -u admin -p admin123 ${PRIVATE_REPO}
    docker build -t ${IMAGE_NAME}:${GO_PIPELINE_COUNTER} .

    echo "*************Image name is [${IMAGE_NAME}:${GO_PIPELINE_COUNTER}]*************"

    docker push ${IMAGE_NAME}:${GO_PIPELINE_COUNTER}
    docker rmi  ${IMAGE_NAME}:${GO_PIPELINE_COUNTER}

    echo "*************Image name is [${IMAGE_NAME}:${GO_PIPELINE_COUNTER}]*************"

}

function test-project {
    docker run --rm -v $(pwd):/opt/app -w /opt/app ${PRIVATE_REPO}/otr/gradle:jdk8 gradle clean test
}

function deploy {
  if [[ ${ENV} == qa ]];then
		sed -i "s#SERVICE_TAG: dev#SERVICE_TAG: qa#g" docker-compose.yml
		sed -i "s#cn#qa#g" docker-compose.yml
	fi

  sed -i "s#${PROJECT_NAME}/${SERVICE_NAME}#${PROJECT_NAME}/${SERVICE_NAME}:${GO_PIPELINE_COUNTER}#g" docker-compose.yml
  sed -i "s#cdm-ibmmq-server.otr-tools#${ENV}-cdm-ibmmq-server.otr-tools#g" docker-compose.yml

	rancher-compose -p ${ENV}-${SERVICE_NAME} up -d -c --upgrade

}

function display-usage {
    echo 'Usage: $ bash go.sh { build-services | deploy }'
}


case $1 in
    build-services)
        build-services
        ;;
    deploy)
        deploy
        ;;
    test-project)
        test-project
        ;;
    *)
        display-usage
        exit -1
esac
