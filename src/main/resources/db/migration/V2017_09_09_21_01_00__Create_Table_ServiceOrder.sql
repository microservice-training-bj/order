CREATE TABLE `service_order` (
  id           VARCHAR(255) PRIMARY KEY,
  `user_id`    VARCHAR(255) NOT NULL,
  `amount`     DOUBLE       NOT NULL,
  `status`     VARCHAR(255) DEFAULT NULL,
  `created_at` DATETIME     NOT NULL,
  `updated_at` DATETIME     NOT NULL
);
