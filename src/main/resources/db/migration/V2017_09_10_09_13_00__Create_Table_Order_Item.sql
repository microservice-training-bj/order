CREATE TABLE `order_item` (
  id           VARCHAR(255) PRIMARY KEY,
  `order_id`    VARCHAR(255) NOT NULL,
  `goods_id`    VARCHAR(255) NOT NULL,
  `quantity`     INT DEFAULT 0,
  CONSTRAINT `fk_order_id` FOREIGN KEY (`order_id`) REFERENCES `service_order` (`id`)
);
