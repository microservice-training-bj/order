CREATE TABLE `shopping_cart` (
  `id`         VARCHAR(255) PRIMARY KEY,
  `user_id`    VARCHAR(255) NOT NULL,
  `goods_id`   VARCHAR(255) NOT NULL,
  `unit_price` DOUBLE       NOT NULL,
  `quantity`   INT
);
