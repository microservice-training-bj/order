package com.thoughtworks.korprulu.order.domain.entity;

import com.thoughtworks.korprulu.order.infrastructure.persistence.po.ShoppingCart;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShoppingCartEntity {

    @Id
    private String id;

    private String goodsId;

    private int quantity;

    private double unitPrice;

    public static ShoppingCartEntity fromShoppingCart(ShoppingCart shoppingCart) {
        return ShoppingCartEntity.builder().goodsId(shoppingCart.getGoodsId())
            .id(shoppingCart.getId())
            .unitPrice(shoppingCart.getUnitPrice())
            .quantity(shoppingCart.getQuantity())
            .build();
    }

    public ShoppingCart toShoppingCart() {
        return ShoppingCart.builder().goodsId(getGoodsId())
            .id(getId())
            .quantity(getQuantity())
            .unitPrice(getUnitPrice())
            .build();
    }
}
