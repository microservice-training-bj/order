package com.thoughtworks.korprulu.order.domain.service;

import com.thoughtworks.korprulu.order.resource.dto.OrderDto;

public interface OrderService {
    OrderDto get(String id);

    OrderDto create(OrderDto orderDto);

    OrderDto update(OrderDto orderDto);

    void delete(String id);
}
