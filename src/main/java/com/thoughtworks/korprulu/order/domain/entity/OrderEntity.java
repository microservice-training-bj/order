package com.thoughtworks.korprulu.order.domain.entity;

import com.thoughtworks.korprulu.order.infrastructure.persistence.po.Order;
import com.thoughtworks.korprulu.order.resource.dto.OrderDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.modelmapper.ModelMapper;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderEntity {

    private String id;

    private String userId;

    private String status;

    private List<OrderItemEntity> orderItems;

    private double amount;

    public OrderDto toDTO() {
        return new ModelMapper().map(this, OrderDto.class);
    }

    public Order toOrder() {
        return new ModelMapper().map(this, Order.class);
    }
}
