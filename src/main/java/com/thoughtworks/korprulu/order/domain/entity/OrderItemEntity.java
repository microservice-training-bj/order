package com.thoughtworks.korprulu.order.domain.entity;

import com.thoughtworks.korprulu.order.infrastructure.persistence.po.OrderItem;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.modelmapper.ModelMapper;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderItemEntity {

    private String orderId;

    private int goodsId;

    private int quantity;

    public OrderItem toOrderItem() {
        return new ModelMapper().map(this, OrderItem.class);
    }

}
