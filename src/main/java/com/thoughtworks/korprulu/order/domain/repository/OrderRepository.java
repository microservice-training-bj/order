package com.thoughtworks.korprulu.order.domain.repository;

import com.thoughtworks.korprulu.order.domain.entity.OrderEntity;

import java.util.List;

public interface OrderRepository {
    OrderEntity get(String id);

    List<OrderEntity> getAll();

    OrderEntity create(OrderEntity order);

    OrderEntity update(OrderEntity order);

    void delete(String id);
}
