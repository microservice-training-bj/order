package com.thoughtworks.korprulu.order.domain.repository;

import com.thoughtworks.korprulu.order.infrastructure.persistence.po.ShoppingCart;

import java.util.List;
import java.util.Set;

public interface ShoppingCartRepository {
    ShoppingCart get(String id);

    ShoppingCart create(ShoppingCart shoppingCart);

    void remove(String id);

    ShoppingCart update(ShoppingCart shoppingCart);

    List<ShoppingCart> findByGoodsIds(Set<String> goodsIds);
}
