package com.thoughtworks.korprulu.order.domain.repository;

import com.thoughtworks.korprulu.order.domain.entity.OrderItemEntity;

public interface OrderItemRepository {
    OrderItemEntity get(String id);

    OrderItemEntity create(OrderItemEntity order);

    OrderItemEntity update(OrderItemEntity order);

    void delete(String id);
}
