package com.thoughtworks.korprulu.order.domain.service.impl;

import com.thoughtworks.korprulu.order.domain.entity.ShoppingCartEntity;
import com.thoughtworks.korprulu.order.domain.repository.ShoppingCartRepository;
import com.thoughtworks.korprulu.order.domain.service.ShoppingCartService;
import com.thoughtworks.korprulu.order.infrastructure.persistence.po.ShoppingCart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    @Override
    public ShoppingCartEntity get(String id) {
        return ShoppingCartEntity.fromShoppingCart(shoppingCartRepository.get(id));
    }

    @Override
    public ShoppingCartEntity create(ShoppingCartEntity shoppingCartEntity) {
        ShoppingCart shoppingCart = shoppingCartEntity.toShoppingCart();
        shoppingCart.setUserId("Extract from token");
        return ShoppingCartEntity.fromShoppingCart(shoppingCartRepository.create(shoppingCart));
    }

    @Override
    public ShoppingCartEntity update(ShoppingCartEntity shoppingCartEntity) {
        ShoppingCart shoppingCart = shoppingCartEntity.toShoppingCart();
        shoppingCart.setUserId("Extract from token");
        return null;
    }

    @Override
    public void delete(String id) {
        shoppingCartRepository.remove(id);
    }
}
