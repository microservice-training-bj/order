package com.thoughtworks.korprulu.order.domain.service;

import com.thoughtworks.korprulu.order.resource.dto.GoodsDto;

public interface GoodsService {
    GoodsDto get(Integer id);
}
