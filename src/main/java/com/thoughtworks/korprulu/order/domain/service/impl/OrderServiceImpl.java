package com.thoughtworks.korprulu.order.domain.service.impl;


import com.thoughtworks.korprulu.order.domain.entity.OrderEntity;
import com.thoughtworks.korprulu.order.domain.repository.OrderItemRepository;
import com.thoughtworks.korprulu.order.domain.repository.OrderRepository;
import com.thoughtworks.korprulu.order.domain.repository.ShoppingCartRepository;
import com.thoughtworks.korprulu.order.domain.service.OrderService;
import com.thoughtworks.korprulu.order.infrastructure.persistence.po.OrderItem;
import com.thoughtworks.korprulu.order.infrastructure.persistence.po.ShoppingCart;
import com.thoughtworks.korprulu.order.resource.dto.OrderDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    @Override
    public OrderDto get(String id) {
        return orderRepository.get(id).toDTO();
    }

    @Override
    public OrderDto create(OrderDto orderDto) {
        List<String> goodsIds = orderDto.getGoodsIds();
        List<ShoppingCart> cartGoods = shoppingCartRepository.findByGoodsIds(new HashSet<>(goodsIds));
        double totalPrice = cartGoods.stream().mapToDouble(ShoppingCart::getUnitPrice).sum();
        List<OrderItem> orderItems = cartGoods.stream().map(goodsItem -> OrderItem.builder()
            .goodsId(goodsItem.getGoodsId())
            .quantity(goodsItem.getQuantity())
            .build()).collect(Collectors.toList());
        OrderEntity order = orderDto.toEntity();
        order.setOrderItems(orderItems.stream().map(OrderItem::toOrderItemEntity).collect(Collectors.toList()));
        order.setAmount(totalPrice);
        orderRepository.create(order);
        return order.toDTO();
    }

    @Override
    public OrderDto update(OrderDto orderDto) {
        return orderRepository.update(orderDto.toEntity()).toDTO();
    }

    @Override
    public void delete(String id) {
        orderRepository.delete(id);
    }
}
