package com.thoughtworks.korprulu.order.domain.service;

import com.thoughtworks.korprulu.order.domain.entity.ShoppingCartEntity;

public interface ShoppingCartService {
    ShoppingCartEntity get(String id);

    ShoppingCartEntity create(ShoppingCartEntity shoppingCartDto);

    ShoppingCartEntity update(ShoppingCartEntity shoppingCartDto);

    void delete(String id);
}
