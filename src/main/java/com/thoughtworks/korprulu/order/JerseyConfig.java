package com.thoughtworks.korprulu.order;

import com.google.common.base.Joiner;
import com.google.common.collect.Sets;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.core.env.Environment;

import java.util.Set;

public abstract class JerseyConfig extends ResourceConfig {
    private static final Class<?>[] COMMON_JERSEY_CLASSES = {
        ApiListingResource.class,
        SwaggerSerializers.class,
    };

    public JerseyConfig(Environment env) {
        Set<Class<?>> classes = Sets.newHashSet(COMMON_JERSEY_CLASSES);
        classes.addAll(getModuleJerseyClasses());
        this.registerClasses(classes);

        initSwaggerBeanConfig(env);
    }

    private void initSwaggerBeanConfig(Environment env) {
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion(env.getProperty("korprulu.jersey.swagger.version", "1.0.0"));
        beanConfig.setSchemes(new String[]{env.getProperty("korprulu.jersey.swagger.scheme", "http")});
        beanConfig.setHost(env.getProperty("korprulu.jersey.swagger.host", "localhost:8080"));
        beanConfig.setBasePath(env.getProperty("korprulu.jersey.swagger.base-path", "/"));
        beanConfig.setResourcePackage(Joiner.on(",").join(getModulePackages()));
        beanConfig.setScan(true);
    }

    // Override for additional jersey classes to register
    protected Set<Class<?>> getModuleJerseyClasses() {
        return Sets.newHashSet();
    }

    // Override for additional jersey classes to register
    protected abstract String[] getModulePackages();
}
