package com.thoughtworks.korprulu.order.helper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.TimeZone;

public class DateUtils {

    public static final String DATE_FOR_ID_FORMAT = "yyMMdd";

    private static final String CHINA_TIME_ZONE = "Asia/Shanghai";

    public static String format(Date date, String pattern) {
        return Optional.ofNullable(date).map(dt -> getDateTimeFormatter(null, pattern).format(dt)).orElse(null);
    }

    private static SimpleDateFormat getDateTimeFormatter(TimeZone tz, String pattern) {
        SimpleDateFormat formatter =
            new SimpleDateFormat(pattern);
        if (tz == null) {
            formatter.setTimeZone(TimeZone.getTimeZone(CHINA_TIME_ZONE));
        }
        return formatter;
    }

    public static String formatNow(String pattern) {
        return format(new Date(), pattern);
    }
}
