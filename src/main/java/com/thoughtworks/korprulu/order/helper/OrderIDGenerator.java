package com.thoughtworks.korprulu.order.helper;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;
import java.util.Random;

public class OrderIDGenerator implements IdentifierGenerator {

    private final Random randomGenerator = new Random();

    public String generateRandomStringNumber() {
        return randomGenerator.ints(6, 0, 10)
            .mapToObj(String::valueOf)
            .reduce(String::concat)
            .get();
    }

    @Override
    public Serializable generate(SessionImplementor session, Object object) throws HibernateException {
        return String.format("%s%s%s",
            object.getClass().getSimpleName().substring(0, 1),
            DateUtils.formatNow(DateUtils.DATE_FOR_ID_FORMAT),
            generateRandomStringNumber());
    }
}
