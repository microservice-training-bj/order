package com.thoughtworks.korprulu.order;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@EnableFeignClients
@SpringCloudApplication
@ComponentScan("com.thoughtworks.korprulu.order")
@EntityScan({
    "com.thoughtworks.korprulu.order.infrastructure.persistence.po"
})
public class KorpruluApplication {
    public static void main(String[] args) {
        new SpringApplicationBuilder(KorpruluApplication.class)
            .bannerMode(Banner.Mode.OFF)
            .run(args);
    }
}
