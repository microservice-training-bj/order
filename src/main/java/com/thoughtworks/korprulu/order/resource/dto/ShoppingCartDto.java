package com.thoughtworks.korprulu.order.resource.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ShoppingCartDto {

    private String id;

    private String userId;

    private String goodsId;

    private int quantity;

    private double unitPrice;

}
