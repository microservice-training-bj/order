package com.thoughtworks.korprulu.order.resource.external;

import com.thoughtworks.korprulu.order.domain.service.GoodsService;

import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name = "goods-service")
public interface GoodsClient extends GoodsService {

}
