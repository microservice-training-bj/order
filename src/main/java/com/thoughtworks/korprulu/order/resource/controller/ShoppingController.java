package com.thoughtworks.korprulu.order.resource.controller;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

import com.thoughtworks.korprulu.order.domain.entity.ShoppingCartEntity;
import com.thoughtworks.korprulu.order.domain.service.ShoppingCartService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/shopping-carts")
public class ShoppingController {

    @Autowired
    private ShoppingCartService shoppingCartService;

    @PostMapping
    @ResponseStatus(CREATED)
    public ShoppingCartEntity create(@RequestBody ShoppingCartEntity shoppingCartEntity) {
        return shoppingCartService.create(shoppingCartEntity);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void delete(@PathVariable String id) {
        shoppingCartService.delete(id);
    }

    @GetMapping("/{id}")
    @ResponseStatus(OK)
    public ShoppingCartEntity get(@PathVariable String id) {
        return shoppingCartService.get(id);
    }

    @PatchMapping("/{id}")
    @ResponseStatus(OK)
    public ShoppingCartEntity update(@RequestBody ShoppingCartEntity shoppingCartEntity) {
        return shoppingCartService.update(shoppingCartEntity);
    }
}
