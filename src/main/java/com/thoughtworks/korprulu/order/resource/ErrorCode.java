package com.thoughtworks.korprulu.order.resource;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorCode {
    UNKNOWN("unknown_error"),
    AUTHENTICATION_FAILED("authentication_failed"),
    SERVICE_REQUEST_ERROR("service_request_error"),
    BAD_REQUEST("bad_request"),
    RESOURCE_NOT_FOUND("resource_not_found");

    private String value;
}
