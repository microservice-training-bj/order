package com.thoughtworks.korprulu.order.resource.controller;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

import com.thoughtworks.korprulu.order.facade.OrderFacade;
import com.thoughtworks.korprulu.order.resource.dto.OrderDto;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/orders")
public class OrderController {

    private OrderFacade facade;

    public OrderController(@Autowired OrderFacade facade) {
        this.facade = facade;
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public OrderDto create(@RequestBody OrderDto orderDto) {
        return facade.create(orderDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void delete(@PathVariable String id) {
        facade.delete(id);
    }

    @GetMapping("/{id}")
    @ResponseStatus(OK)
    public OrderDto get(@PathVariable String id) {
        return facade.get(id);
    }

    @PatchMapping("/{id}")
    @ResponseStatus(OK)
    public OrderDto update(@RequestBody OrderDto orderDto) {
        return facade.update(orderDto);
    }
}
