package com.thoughtworks.korprulu.order.resource.dto;

import com.thoughtworks.korprulu.order.domain.entity.OrderEntity;
import com.thoughtworks.korprulu.order.domain.entity.OrderItemEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {

    private String id;
    private String userId;
    private List<String> goodsIds;

    public OrderEntity toEntity() {
        return new ModelMapper().map(this, OrderEntity.class);
    }
}
