package com.thoughtworks.korprulu.order;

import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.util.Set;

@Configuration
@ComponentScan("com.thoughtworks.korprulu.common.jersey")
public class ServiceJerseyConfig extends JerseyConfig {

    @Autowired
    public ServiceJerseyConfig(Environment env) {
        super(env);
    }

    @Override
    protected Set<Class<?>> getModuleJerseyClasses() {
        return Sets.newHashSet(
        );
    }

    @Override
    protected String[] getModulePackages() {
        return new String[]{
            "com.thoughtworks.korprulu.order.resource",
        };
    }
}
