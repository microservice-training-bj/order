package com.thoughtworks.korprulu.order.infrastructure.persistence.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@Table(name = "shopping_cart")
public class ShoppingCart {

    @Id
    @GenericGenerator(name = "id_generator", strategy = "com.thoughtworks.korprulu.order.helper.RandomIDGenerator")
    @GeneratedValue(generator = "id_generator")
    private String id;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "goods_id")
    private String goodsId;

    private int quantity;

    @Column(name = "unit_price")
    private double unitPrice;

}
