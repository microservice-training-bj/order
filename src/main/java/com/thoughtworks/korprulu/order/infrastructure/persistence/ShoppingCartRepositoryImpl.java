package com.thoughtworks.korprulu.order.infrastructure.persistence;

import com.thoughtworks.korprulu.order.domain.repository.ShoppingCartRepository;
import com.thoughtworks.korprulu.order.infrastructure.persistence.po.ShoppingCart;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface ShoppingCartRepositoryImpl extends JpaRepository<ShoppingCart, String>, ShoppingCartRepository {
    List<ShoppingCart> findByGoodsIdIn(Set<String> goodsId);

    @Override
    default ShoppingCart get(String id) {
        return findOne(id);
    }

    @Override
    default ShoppingCart create(ShoppingCart shoppingCart) {
        return save(shoppingCart);
    }

    @Override
    default void remove(String id) {
        delete(id);
    }

    @Override
    default ShoppingCart update(ShoppingCart shoppingCart) {
        return save(shoppingCart);
    }

    @Override
    default List<ShoppingCart> findByGoodsIds(Set<String> goodsIds) {
        return findByGoodsIdIn(goodsIds);
    }
}
