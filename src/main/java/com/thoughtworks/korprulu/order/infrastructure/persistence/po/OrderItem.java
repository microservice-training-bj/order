package com.thoughtworks.korprulu.order.infrastructure.persistence.po;

import com.thoughtworks.korprulu.order.domain.entity.OrderItemEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.modelmapper.ModelMapper;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@AllArgsConstructor
@Builder
@NoArgsConstructor
@Table(name = "order_item")
public class OrderItem {

    @Id
    @GenericGenerator(name = "id_generator", strategy = "com.thoughtworks.korprulu.order.helper.RandomIDGenerator")
    @GeneratedValue(generator = "id_generator")
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    private Order order;

    @Column(name = "goods_id")
    @NotNull
    private String goodsId;

    @Column(name = "quantity")
    private Integer quantity;

    public OrderItemEntity toOrderItemEntity() {
        return new ModelMapper().map(this, OrderItemEntity.class);
    }

}
