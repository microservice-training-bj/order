package com.thoughtworks.korprulu.order.infrastructure.persistence;

import com.thoughtworks.korprulu.order.domain.entity.OrderEntity;
import com.thoughtworks.korprulu.order.domain.repository.OrderRepository;
import com.thoughtworks.korprulu.order.infrastructure.persistence.po.Order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public interface OrderRepositoryImpl extends JpaRepository<Order, String>, OrderRepository {

    void deleteById(String id);

    @Override
    default OrderEntity create(OrderEntity orderEntity) {
        return save(orderEntity.toOrder()).toOrderEntity();
    }

    @Override
    default OrderEntity get(String id) {
        return findOne(id).toOrderEntity();
    }

    @Override
    default List<OrderEntity> getAll() {
        return findAll().stream().map(Order::toOrderEntity).collect(Collectors.toList());
    }

    @Override
    default OrderEntity update(OrderEntity order) {
        return this.save(order.toOrder()).toOrderEntity();
    }

    @Override
    default void delete(String id) {
        this.deleteById(id);
    }
}
