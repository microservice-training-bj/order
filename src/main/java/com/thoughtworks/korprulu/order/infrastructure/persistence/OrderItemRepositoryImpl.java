package com.thoughtworks.korprulu.order.infrastructure.persistence;

import com.thoughtworks.korprulu.order.domain.entity.OrderItemEntity;
import com.thoughtworks.korprulu.order.domain.repository.OrderItemRepository;
import com.thoughtworks.korprulu.order.infrastructure.persistence.po.OrderItem;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderItemRepositoryImpl extends JpaRepository<OrderItem, String>, OrderItemRepository {

    void deleteById(String id);

    default OrderItemEntity get(String id) {
        return findOne(id).toOrderItemEntity();
    }

    default OrderItemEntity create(OrderItemEntity order) {
        return save(order.toOrderItem()).toOrderItemEntity();
    }

    default OrderItemEntity update(OrderItemEntity order) {
        return save(order.toOrderItem()).toOrderItemEntity();
    }

    default void delete(String id) {
        deleteById(id);
    }
}
