package com.thoughtworks.korprulu.order.facade;

import com.thoughtworks.korprulu.order.domain.service.OrderService;
import com.thoughtworks.korprulu.order.resource.dto.OrderDto;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class OrderFacade {

    private OrderService orderService;

    public OrderFacade(@Autowired OrderService orderService) {
        this.orderService = orderService;
    }

    public OrderDto create(OrderDto orderDto) {
        return orderService.create(orderDto);
    }

    public void delete(String id) {
        orderService.delete(id);
    }

    public OrderDto get(String id) {
        return orderService.get(id);
    }

    public OrderDto update(OrderDto orderDto) {
        return orderService.update(orderDto);
    }

}
