package com.thoughtworks.korprulu.order.resource;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.thoughtworks.korprulu.APIBaseTest;
import com.thoughtworks.korprulu.order.domain.repository.OrderRepository;
import com.thoughtworks.korprulu.order.helper.JsonUtils;
import com.thoughtworks.korprulu.order.resource.dto.OrderDto;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.util.Arrays;

public class OrderControllerTest extends APIBaseTest {

    @Autowired
    private OrderRepository orderRepository;

    @Test
    @Ignore
    public void shouldCreateOrder() throws Exception {
        String existingUserId = "90fdjafp4oa09fasdofsa";
        mockMvc.perform(post("/api/orders")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(JsonUtils.marshal(OrderDto.builder()
                .userId(existingUserId)
                .goodsIds(Arrays.asList("goodsId1", "goodsId2", "goodsId3", "goodsId4"))
                .build())))
            .andExpect(status().isCreated());

        assertEquals(1, orderRepository.getAll().size());
        assertEquals(existingUserId, orderRepository.getAll().get(0).getUserId());
    }

}
