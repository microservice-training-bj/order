package com.thoughtworks.korprulu;

import com.thoughtworks.korprulu.functional.BaseFunctionalTest;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@Transactional
@EnableJpaAuditing
public abstract class APIBaseTest extends BaseFunctionalTest {

    protected MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

}
